![nox-logo](https://gitlab.com/marcoschaefer/nox-plugin/-/wikis/uploads/38b6b149372fefd009184ca8d7b8e1a7/nox-logo.png)
# Nox Plugin

[![pipeline status](https://gitlab.com/marcoschaefer/nox-plugin/badges/development/pipeline.svg)](https://gitlab.com/marcoschaefer/nox-plugin/-/pipelines)

This project aimed to apply my contact with Kotlin, test my knowledge and solve a problem I created. When I went to build a minecraft server the best projects I found to customize the MOTD had some problem so I decided to create my own.

Feel free to comment and suggest improvements.

## Installation

After clone the projet, go to root and:

```
./gradlew clean build
```

build jar:

```
./gradlew jar
```
Ps: The jar will be generated on `nox-plugin\build\lib`

Put this jar in the plugin folder of your minecraft server and voila

## License
This project is under the [GNU PGLv3](https://gitlab.com/marcoschaefer/nox-plugin/-/blob/development/LICENSE)

