plugins {

    java
    application

    // Maven
    id("maven-publish")

    kotlin("jvm") version "1.6.0"
    id("com.github.johnrengelman.shadow") version "7.1.0"
}

application {
    mainClass.set("br.com.schaefer.NoxKt")
}

repositories {
    maven(url = "https://oss.sonatype.org/content/repositories/snapshots/") {
        name = "sonatype-oss-snapshots"
    }
    mavenCentral()
    maven("https://hub.spigotmc.org/nexus/content/repositories/snapshots/")
    maven("https://repo.dmulloy2.net/repository/public/")
}

allprojects {
    group = "br.com.schaefer"
    version = "1.1.0".withDynamicSuffix()
}

java.sourceCompatibility = JavaVersion.VERSION_16

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib:1.6.0")
    implementation("org.bstats:bstats-bukkit:2.2.1")

    compileOnly("org.spigotmc:spigot-api:1.17-R0.1-SNAPSHOT")
    compileOnly("com.comphenix.protocol:ProtocolLib:4.7.0")
}

fun String.withDynamicSuffix(): String {
    val branch = System.getenv("CI_BUILD_REF_NAME")
    val tag = """^v[\d]*.[\d]*.[\d]*$""".toRegex()

    val suffix = if (branch == null || branch.isBlank() || "development" == branch) "-SNAPSHOT" // Development suffix
    else if (branch != "master" && !tag.matches(branch)) "-$branch-SNAPSHOT" // Other branches suffix
    else if (branch == "master") "-Final" // Master suffix
    else "" // Tag suffix

    return plus(suffix)
}

tasks {
    shadowJar {
        relocate("org.bstats", "br.com.schaefer.nox.bstats")
    }
    jar {
        manifest {
            attributes["Main-Class"] = "br.com.schaefer.NoxKt"
        }

        doFirst {
            archiveVersion.set(project.version.toString())
            val jarFiles = mutableMapOf<String, Int>()

            rename {
                if (!it.endsWith(".jar")) return@rename it

                val count = jarFiles
                    .computeIfPresent(it) {_, value -> value + 1}
                    ?: jarFiles.computeIfAbsent(it) { 1 }

                it.replace(".jar", "-$count.jar")
            }
        }

        // To add all dependencies
        from(sourceSets.main.get().output)

        dependsOn(configurations.runtimeClasspath, shadowJar)
        from({
            configurations.runtimeClasspath.get().filter { it.name.endsWith("jar") }.map { zipTree(it) }
        })
    }
}

// Maven local publishing
val sourceJar by tasks.creating(Jar::class) {
    manifest {
        attributes["Main-Class"] = "br.com.schaefer.NoxKt"
    }
    archiveClassifier.set("sources")
    from(sourceSets.main.get().allSource)

    dependsOn(configurations.runtimeClasspath, tasks.shadowJar)
    from({
        configurations.runtimeClasspath.get().filter { it.name.endsWith("jar") }.map { zipTree(it) }
    })
}

artifacts {
    archives(sourceJar)
}

publishing {
    publications {
        create<MavenPublication>("mavenJava") {
            from(components["java"])
            artifact(sourceJar)
        }
    }
    repositories {
        val api = System.getenv("CI_API_V4_URL")
        val projectId = System.getenv("CI_PROJECT_ID")
        val token = System.getenv("CI_JOB_TOKEN")
        maven {
            url = uri("$api/projects/$projectId/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                name = "Job-Token"
                value = token
            }
            authentication {
                create<HttpHeaderAuthentication>("header")
            }
        }
    }
}

tasks.named<Copy>("processResources") {
    inputs.property("name", project.name)
    inputs.property("version", version)
    filesMatching("plugin.yml") {
        expand(
            "name" to project.name,
            "version" to version)
    }
    filesMatching("project.properties") {
        expand(
            "name" to project.name,
            "version" to version)
    }
}
