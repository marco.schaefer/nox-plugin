package br.com.schaefer.nox

import br.com.schaefer.nox.Constants.BSTATS_PROJECT_ID
import br.com.schaefer.nox.Constants.CONFIG_FILE_NAME
import br.com.schaefer.nox.Constants.PROJECT_PROPERTIES_FILE_NAME
import br.com.schaefer.nox.Constants.PROTOCOL_LIB_NAME
import br.com.schaefer.nox.adapters.NoxAdapter
import br.com.schaefer.nox.commands.NoxCommand
import br.com.schaefer.nox.commands.NoxCompleter
import br.com.schaefer.nox.litenters.PingListener
import br.com.schaefer.nox.litenters.PlayerJoinListener
import com.comphenix.protocol.PacketType.Status.Server.SERVER_INFO
import com.comphenix.protocol.ProtocolLibrary
import com.comphenix.protocol.events.PacketAdapter
import org.bstats.bukkit.Metrics
import org.bukkit.plugin.java.JavaPlugin
import java.io.*
import java.util.*
import java.util.logging.Level


class Nox : JavaPlugin() {

    private val projectProperties = Properties()

    companion object {
        @JvmStatic lateinit var instance: Nox
    }

    init {
        projectProperties.load(this.javaClass.getResourceAsStream("/$PROJECT_PROPERTIES_FILE_NAME"))
    }

    override fun onLoad() {
        super.onLoad()
        loadConfig()
    }

    fun loadConfig() {
        super.onLoad()
        dataFolder.mkdirs()
        createDefaultConfiguration()

        config.load(File(dataFolder, CONFIG_FILE_NAME))
    }

    override fun onEnable() {
        instance = this

        // Register listeners
        server.pluginManager.registerEvents(PlayerJoinListener(), this)
        server.pluginManager.registerEvents(PingListener(), this)
        if (server.pluginManager.isPluginEnabled(PROTOCOL_LIB_NAME)) {
            ProtocolLibrary
                .getProtocolManager()
                .addPacketListener(
                    NoxAdapter(PacketAdapter.params(this, SERVER_INFO).optionAsync()))
        } else {
            logger.warning("$PROTOCOL_LIB_NAME could not be loaded! You only have access to a reduced feature set.")
        }

        //Register command
        getCommand(Constants.COMMAND_NAME)!!.setExecutor(NoxCommand())
        getCommand(Constants.COMMAND_NAME)!!.tabCompleter = NoxCompleter()

        // Check update
        UpdateChecker().checkUpdate()

        //Enabled bStats
        Metrics(this, BSTATS_PROJECT_ID)
    }

    private fun createDefaultConfiguration() {
        val actual = File(dataFolder, CONFIG_FILE_NAME)
        if (!actual.exists()) {
            try {
                getResource("defaults/$CONFIG_FILE_NAME").use { stream ->
                    if (stream == null) {
                        throw FileNotFoundException()
                    }
                    copyDefaultConfig(stream, actual)
                }
            } catch (e: IOException) {
                logger.severe("Unable to read default configuration: $CONFIG_FILE_NAME")
            }
        }
    }

    private fun copyDefaultConfig(input: InputStream, actual: File) {
        try {
            FileOutputStream(actual).use { output ->
                val buf = ByteArray(8192)
                var length: Int
                while (input.read(buf).also { length = it } > 0) {
                    output.write(buf, 0, length)
                }
                logger.info("Default configuration file written: $CONFIG_FILE_NAME")
            }
        } catch (e: IOException) {
            logger.log(Level.WARNING, "Failed to write default config file", e)
        }
    }
}