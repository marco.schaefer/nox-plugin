package br.com.schaefer.nox

class Version(private val major: Int,
              private val minor: Int,
              private val patch: Int,
              private val suffix: String?) : Comparable<Version> {

    companion object {
        fun fromString(str: String) : Version? {
            val regex = """(?<major>\d*)\.(?<minor>\d*)\.(?<patch>\d*)(?<suffix>-[\W\w]*)?""".toRegex()

            val find = regex.find(str)

            return if (find != null) {
                val (major, minor, patch, suffix) = find.destructured
                Version(major.toInt(), minor.toInt(), patch.toInt(), suffix)
            } else null
        }
    }

    override fun compareTo(other: Version): Int = when {
        this.major != other.major -> this.major compareTo other.major
        this.minor != other.minor -> this.minor compareTo other.minor
        this.patch != other.patch -> this.patch compareTo other.patch
        else -> 0
    }

    override fun toString() = "$major.$minor.$patch${suffix ?: ""}"

}