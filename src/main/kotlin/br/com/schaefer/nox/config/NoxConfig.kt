package br.com.schaefer.nox.config

import br.com.schaefer.nox.Constants.CONFIG_PREFIX
import br.com.schaefer.nox.Nox

enum class NoxConfig(key: String, private val default: Any?) {

    CHECK_UPDATE_JOIN("check-update-join", false),

    CLASSIC_MOTD("classic.motd", null),

    RANDOM_MOTD_ENABLED("random.enabled", false),
    RANDOM_MOTD("random.motd", null),

    BAN_MOTD_ENABLED("ban.enabled", true),
    TEMPORARY_BAN_MOTD("ban.temporary-ban-motd", null),
    PERMANENT_BAN_MOTD("ban.permanent-ban-motd", null),

    FORMAT_EXPIRATION_DATE("format.expiration-date", null),
    FORMAT_DATE("format.date", null),
    FORMAT_TIME("format.time", null),

    SLOTS_HIDDEN("slots.hidden", false),

    FAKE_SERVER_SLOTS_ENABLED("slots.fake-slots.enabled", false),
    FAKE_SERVER_SLOTS("slots.fake-slots.value", null),

    FAKE_ONLINE_FIXED_ENABLED("slots.fake-online-fixed.enabled", false),
    FAKE_ONLINE_FIXED("slots.fake-online-fixed.value", null),

    FAKE_ONLINE_PLUS_ENABLED("slots.fake-online-plus.enabled", false),
    FAKE_ONLINE_PLUS("slots.fake-online-plus.plus", null),

    FAKE_ONLINE_MULTIPLY_ENABLED("slots.fake-online-multiply.enabled", false),
    FAKE_ONLINE_MULTIPLY("slots.fake-online-multiply.multiply", null),

    CUSTOM_SLOTS_ENABLED("slots.custom.enabled", false),
    CUSTOM_SLOTS("slots.custom.text", null),

    CUSTOM_TOOLTIP_SLOTS_ENABLED("slots.tooltip.enabled", false),
    CUSTOM_TOOLTIP_SLOTS("slots.tooltip.message", null),

    CUSTOM_VERSION_ENABLED("version.enabled", false),
    CUSTOM_VERSION("version.text", null),

    CUSTOM_PLACEHOLDERS("placeholders", null),
    ;

    val keyWithPrefix = "$CONFIG_PREFIX.$key"

    @Suppress("UNCHECKED_CAST")
    fun <T> getValue(): T? = Nox.instance.config.get(keyWithPrefix, default)?.let { it as T }

}