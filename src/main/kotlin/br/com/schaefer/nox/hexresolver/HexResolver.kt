package br.com.schaefer.nox.hexresolver

import net.md_5.bungee.api.ChatColor
import java.awt.Color
import java.util.*
import java.util.regex.Pattern
import java.util.stream.Collectors

/**
 * Credits go to harry0198 https://github.com/harry0198/HexiTextLib MIT License
 */
class HexResolver {

    private val gradientPattern = Pattern.compile("<(gradient|g)(:#([a-fA-F0-9]){6})+>")
    private val hexPattern = Pattern.compile("<(#[a-fA-F0-9]{6})>")


    /**
     * Checks if hex colour codes are supported for the version of minecraft.
     * @return True if supports hex. False if not.
     */
    private fun serverSupportsHex(): Boolean {
        return try {
            ChatColor.of(Color.BLACK)
            true
        } catch (ignore: NoSuchMethodError) {
            false
        }
    }

    /**
     * Parses string into hex colours where applicable using custom pattern matcher
     * @param text String to parse into hex
     * @return Parsed Hex where applicable otherwise returns inputted string
     */
    private fun parseHexString(text: String, hexPattern: Pattern): String {
        var textParse = text
        var hexColorMatcher = hexPattern.matcher(textParse)
        if (serverSupportsHex()) {
            textParse = parseGradients(textParse)
            while (hexColorMatcher.find()) {
                val hex = hexColorMatcher.group(1)
                val color = ChatColor.of(hex)
                val before = textParse.substring(0, hexColorMatcher.start())
                val after = textParse.substring(hexColorMatcher.end())
                textParse = before + color + after
                hexColorMatcher = hexPattern.matcher(textParse)
            }
        }
        return org.bukkit.ChatColor.translateAlternateColorCodes('&', textParse)
    }

    /**
     * Parses string into hex colours where applicable using default pattern matcher
     * @param text String to parse into hex
     * @return Parsed Hex where applicable otherwise returns inputted string
     */
    fun parseHexString(text: String): String {
        return parseHexString(text, hexPattern)
    }

    /**
     * Parses string into a gradient colour coded string
     * @param text String to parse
     * @return String with gradient applied
     */
    private fun parseGradients(text: String): String {
        var parsed = text
        var matcher = gradientPattern.matcher(parsed)
        while (matcher.find()) {
            val parsedGradient = StringBuilder()
            val match = matcher.group()
            val tagLength = if (match.startsWith("<gr")) 10 else 3
            val indexOfClose = match.indexOf(">")
            val hexContent = match.substring(tagLength, indexOfClose)
            val hexSteps = Arrays.stream(hexContent.split(":").toTypedArray())
                .map { Color.decode(it) }
                .collect(Collectors.toList())
            val stop = findGradientStop(parsed, matcher.end())
            val content = parsed.substring(matcher.end(), stop)

            val gradient = Gradient(hexSteps, content.length)
            var tempFormat = ""
            for (c in content.toCharArray()) {
                if (c != '§' && tempFormat !== "§") {
                    // This is a normal char
                    parsedGradient
                        .append(ChatColor.of(gradient.next()).toString())
                        .append(tempFormat)
                        .append(c)
                } else if (c == '§') {
                    // A custom formatting is starting
                    tempFormat = "§"
                } else if (c != '§' && tempFormat.contains("§")) {
                    // Type of custom formatting defined now
                    tempFormat += c
                }
            }
            val before = parsed.substring(0, matcher.start())
            val after = parsed.substring(stop)
            parsed = before + parsedGradient + after
            matcher = gradientPattern.matcher(parsed)
        }
        return parsed
    }

    /**
     * Returns the index of the colour that is about to change to the next
     *
     * @param content The content to search through
     * @param searchAfter The index at which to search after
     * @return the index of the color stop, or the end of the string index if none is found
     */
    private fun findGradientStop(content: String, searchAfter: Int): Int {
        val matcher = gradientPattern.matcher(content)
        while (matcher.find()) {
            if (matcher.start() > searchAfter) return matcher.start()
        }
        return content.length - 1
    }
}