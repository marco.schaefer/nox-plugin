package br.com.schaefer.nox.hexresolver

import java.awt.Color
/**
 * Credits go to harry0198 https://github.com/harry0198/HexiTextLib MIT License
 *
 * Gradient class
 * Derived from:
 * https://github.com/Rosewood-Development/RoseStacker/blob/master/Plugin/src/main/java/dev/rosewood/rosestacker/utils/HexUtils.java
 * https://github.com/Oribuin/ChatEmojis/blob/master/src/main/java/xyz/oribuin/chatemojis/utils/HexUtils.java
 *
 * Unsure who original author is.
 * @author unknown
 */
class Gradient(private val colors: List<Color>, totalColors: Int) {

    private var stepSize = 0
    private var step = 0
    private var stepIndex = 0

    init {
        require(colors.size >= 2) { "Must provide at least 2 colors" }
        require(totalColors >= 1) { "Must have at least 1 total color" }
        stepSize = totalColors / (colors.size - 1)
        this.stepIndex = 0
        step = this.stepIndex
    }

    /**
     * @return the next color in the gradient
     */
    operator fun next(): Color {
        val color = if (this.stepIndex + 1 < colors.size) {
            val start = colors[this.stepIndex]
            val end = colors[this.stepIndex + 1]
            val interval = step.toFloat() / stepSize
            getGradientInterval(start, end, interval)
        } else {
            colors[colors.size - 1]
        }
        step += 1
        if (step >= stepSize) {
            step = 0
            this.stepIndex++
        }
        return color
    }

    /**
     * Gets a color along a linear gradient between two colors
     *
     * @param start The start color
     * @param end The end color
     * @param interval The interval to get, between 0 and 1 inclusively
     * @return A Color at the interval between the start and end colors
     */
    private fun getGradientInterval(start: Color, end: Color, interval: Float): Color {
        require(!(0 > interval || interval > 1)) { "Interval must be between 0 and 1 inclusively." }
        val r = (end.red * interval + start.red * (1 - interval)).toInt()
        val g = (end.green * interval + start.green * (1 - interval)).toInt()
        val b = (end.blue * interval + start.blue * (1 - interval)).toInt()
        return Color(r, g, b)
    }
}