package br.com.schaefer.nox.providers

import br.com.schaefer.nox.Nox
import br.com.schaefer.nox.config.NoxConfig

class PlayersOnlineProvider {

    private val fakeOnlineFixEnabled = NoxConfig.FAKE_ONLINE_FIXED_ENABLED.getValue<Boolean>() ?: false
    private val fakeOnlineFix = NoxConfig.FAKE_ONLINE_FIXED.getValue<Int>()

    private val fakeOnlinePlusEnabled = NoxConfig.FAKE_ONLINE_PLUS_ENABLED.getValue<Boolean>() ?: false
    private val fakeOnlinePlus = NoxConfig.FAKE_ONLINE_PLUS.getValue<Int>()

    private val fakeOnlineMultiplyEnabled = NoxConfig.FAKE_ONLINE_MULTIPLY_ENABLED.getValue<Boolean>() ?: false
    private val fakeOnlineMultiply = NoxConfig.FAKE_ONLINE_MULTIPLY.getValue<Int>()

    private val playersOnline = Nox.instance.server.onlinePlayers.size
    private val maxPlayers = ServerSlotsProvider().getFakeSlots()

    fun getFakeOnlinePlayer() = (if (fakeOnlineMultiplyEnabled) playersOnline * (fakeOnlineMultiply ?: 1)
        else if (fakeOnlinePlusEnabled) playersOnline + (fakeOnlinePlus ?: 0)
        else if (fakeOnlineFixEnabled) (fakeOnlineFix ?: playersOnline)
        else playersOnline)
        .let { if (it > maxPlayers) maxPlayers else it }

}