package br.com.schaefer.nox.providers

import br.com.schaefer.nox.Nox
import br.com.schaefer.nox.config.NoxConfig

class ServerSlotsProvider {

    private val enabled = NoxConfig.FAKE_SERVER_SLOTS_ENABLED.getValue<Boolean>() ?: false
    private val fakeMax = NoxConfig.FAKE_SERVER_SLOTS.getValue<Int>()

    private val maxPlayers = Nox.instance.server.maxPlayers

    fun getFakeSlots(): Int = if (enabled) (fakeMax ?: maxPlayers) else maxPlayers
}