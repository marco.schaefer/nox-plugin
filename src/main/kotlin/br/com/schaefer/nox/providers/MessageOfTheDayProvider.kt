package br.com.schaefer.nox.providers

import br.com.schaefer.nox.motd.BannedMessageOfTheDay
import br.com.schaefer.nox.motd.ClassicMessageOfTheDay
import br.com.schaefer.nox.motd.MessageOfTheDay
import br.com.schaefer.nox.motd.RandomMessageOfTheDay
import org.bukkit.event.server.ServerListPingEvent

class MessageOfTheDayProvider {

    fun getMessageOfTheDay(event : ServerListPingEvent): MessageOfTheDay {
        return listOf(BannedMessageOfTheDay(event), RandomMessageOfTheDay(event), ClassicMessageOfTheDay(event))
            .find { it.ableToApply }!!
    }

}