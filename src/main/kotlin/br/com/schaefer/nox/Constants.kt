package br.com.schaefer.nox

object Constants {

    const val COMMAND_NAME = "nox"
    const val CONFIG_PREFIX = "nox"

    const val CONFIG_FILE_NAME= "config.yml"
    const val PROJECT_PROPERTIES_FILE_NAME= "project.properties"

    const val PROTOCOL_LIB_NAME = "ProtocolLib"

    const val COURSEFORGE_API_URI = "https://api.curseforge.com"
    const val COURSEFORGE_BUKKIT_PLUGINS_URI = "https://www.curseforge.com/minecraft/bukkit-plugins"
    const val COURSEFORGE_PROJECT_URI = "nox-plugin"
    const val COURSEFORGE_PROJECT_ID = "549125"
    const val BSTATS_PROJECT_ID = 13447

}