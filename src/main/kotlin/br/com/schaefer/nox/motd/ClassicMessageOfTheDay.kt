package br.com.schaefer.nox.motd

import br.com.schaefer.nox.config.NoxConfig
import org.bukkit.event.server.ServerListPingEvent

class ClassicMessageOfTheDay(event: ServerListPingEvent) : MessageOfTheDay(event) {

    override val ableToApply = true

    override fun getMessage() = NoxConfig.CLASSIC_MOTD.getValue<String>()

}