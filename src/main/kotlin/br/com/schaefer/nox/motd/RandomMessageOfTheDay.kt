package br.com.schaefer.nox.motd

import br.com.schaefer.nox.config.NoxConfig
import org.bukkit.event.server.ServerListPingEvent
import kotlin.random.Random

class RandomMessageOfTheDay(event: ServerListPingEvent) : MessageOfTheDay(event) {

    override val ableToApply = NoxConfig.RANDOM_MOTD_ENABLED.getValue<Boolean>() ?: false
    override fun getMessage() = NoxConfig.RANDOM_MOTD.getValue<ArrayList<String>>()
        ?.let{ it[Random.nextInt(0, it.size - 1)]}

}