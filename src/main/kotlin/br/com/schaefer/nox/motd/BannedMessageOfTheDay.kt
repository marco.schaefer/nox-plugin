package br.com.schaefer.nox.motd

import br.com.schaefer.nox.Nox
import br.com.schaefer.nox.config.NoxConfig
import br.com.schaefer.nox.processors.placeholders.BanPlaceholderProcessor
import org.bukkit.BanEntry
import org.bukkit.BanList.Type.IP
import org.bukkit.event.server.ServerListPingEvent

class BannedMessageOfTheDay(event: ServerListPingEvent) : MessageOfTheDay(event) {

    private val banEntry : BanEntry? = findBanEntry()
    override val ableToApply = (NoxConfig.BAN_MOTD_ENABLED.getValue<Boolean>() ?: false) && ipIsBanned()

    override fun getMessage() = if (banEntry!!.expiration != null) getTemporaryBanMessage() else getPermanentlyBanMessage()

    override fun getCustomProcessMessage() = BanPlaceholderProcessor(banEntry!!)

    private fun findBanEntry() = Nox.instance.server.getBanList(IP).banEntries.find { it.target == event.address.hostName }

    private fun ipIsBanned() = banEntry != null

    private fun getTemporaryBanMessage() = NoxConfig.TEMPORARY_BAN_MOTD.getValue<String>()

    private fun getPermanentlyBanMessage() = NoxConfig.PERMANENT_BAN_MOTD.getValue<String>()

}