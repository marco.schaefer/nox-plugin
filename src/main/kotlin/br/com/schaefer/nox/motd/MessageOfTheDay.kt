package br.com.schaefer.nox.motd

import br.com.schaefer.nox.processors.TextFormatProcessor
import br.com.schaefer.nox.processors.placeholders.PlaceholderProcessor
import org.bukkit.event.server.ServerListPingEvent

abstract class MessageOfTheDay(val event : ServerListPingEvent) {

    abstract val ableToApply : Boolean

    open fun getCustomProcessMessage() : PlaceholderProcessor? = null

    abstract fun getMessage() : String?

    fun getFormattedMessage() : String {
        return TextFormatProcessor().formatText(getMessage(), getCustomProcessMessage())
    }

}