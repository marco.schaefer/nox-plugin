package br.com.schaefer.nox

object PlaceholderKeyword {

    // System keywords
    const val DATE = "date"
    const val TIME = "time"
    const val DATETIME = "dateTime"
    const val SERVER_NAME = "serverName"
    const val SERVER_VERSION = "serverVersion"
    const val BUKKIT_VERSION = "bukkitVersion"
    const val PLAYERS_BANS = "playersBans"
    const val IP_BANS = "ipBans"
    const val PLAYERS_ONLINE = "playersOnline"
    const val SLOTS = "slots"

    // Ban keywords
    const val BAN_REASON = "banReason"
    const val BAN_DATE = "banDate"
    const val BAN_SOURCE = "banSource"
    const val BAN_EXPIRES = "banExpires"

}