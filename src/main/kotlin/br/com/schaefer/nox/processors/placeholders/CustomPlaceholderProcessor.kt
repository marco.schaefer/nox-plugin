package br.com.schaefer.nox.processors.placeholders

import br.com.schaefer.nox.config.NoxConfig
import org.bukkit.configuration.MemorySection
import java.util.function.Function

class CustomPlaceholderProcessor : PlaceholderProcessor() {

    private val regex = """-(?<letter>\w)""".toRegex()
    private val reverseRegex = """[A-Z]""".toRegex()

    init {
        val placeholders = NoxConfig.CUSTOM_PLACEHOLDERS.getValue<MemorySection>()

        replaces.addAll(placeholders?.getKeys(true)
            ?.map { getPlaceholder(buildKey(it)) to "${placeholders.get(it)}"}
            ?.map { pair -> Function { defaultReplace(pair.first, pair.second, it)} } ?: emptyList())
    }

    fun buildKey(key: String) = regex.replace(key) {it.value.uppercase()}.replace("-", "")

    fun buildReverseKey(key: String) = reverseRegex.replace(key) { "-${it.value.lowercase()}" }

}