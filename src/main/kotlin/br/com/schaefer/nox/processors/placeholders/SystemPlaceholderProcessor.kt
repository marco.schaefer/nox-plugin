package br.com.schaefer.nox.processors.placeholders

import br.com.schaefer.nox.Nox
import br.com.schaefer.nox.PlaceholderKeyword.BUKKIT_VERSION
import br.com.schaefer.nox.PlaceholderKeyword.DATE
import br.com.schaefer.nox.PlaceholderKeyword.DATETIME
import br.com.schaefer.nox.PlaceholderKeyword.IP_BANS
import br.com.schaefer.nox.PlaceholderKeyword.PLAYERS_BANS
import br.com.schaefer.nox.PlaceholderKeyword.PLAYERS_ONLINE
import br.com.schaefer.nox.PlaceholderKeyword.SERVER_NAME
import br.com.schaefer.nox.PlaceholderKeyword.SERVER_VERSION
import br.com.schaefer.nox.PlaceholderKeyword.SLOTS
import br.com.schaefer.nox.PlaceholderKeyword.TIME
import br.com.schaefer.nox.config.NoxConfig
import br.com.schaefer.nox.providers.PlayersOnlineProvider
import br.com.schaefer.nox.providers.ServerSlotsProvider
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.function.Function

class SystemPlaceholderProcessor : PlaceholderProcessor() {

    private val server = Nox.instance.server

    private val datePair = getPlaceholder(DATE) to LocalDateTime.now()
    private val timePair = getPlaceholder(TIME) to LocalDateTime.now()
    private val dateTimePair = getPlaceholder(DATETIME) to LocalDateTime.now()
    private val serverNamePair = getPlaceholder(SERVER_NAME) to server.name
    private val serverVersionPair = getPlaceholder(SERVER_VERSION) to server.version
    private val bukkitVersionPair = getPlaceholder(BUKKIT_VERSION) to server.bukkitVersion
    private val playersBansPair = getPlaceholder(PLAYERS_BANS) to server.bannedPlayers.size
    private val ipBansPair = getPlaceholder(IP_BANS) to server.ipBans.size
    private val playersOnlinePair = getPlaceholder(PLAYERS_ONLINE) to PlayersOnlineProvider().getFakeOnlinePlayer()
    private val playersSlotsPair = getPlaceholder(SLOTS) to ServerSlotsProvider().getFakeSlots()

    init {
        val formatDate = NoxConfig.FORMAT_DATE.getValue<String>() ?: ""
        val formatTime = NoxConfig.FORMAT_TIME.getValue<String>() ?: ""
        val formatDateTime = "$formatDate $formatTime"

        replaces.add(Function { replaceDate(datePair.first, datePair.second, formatDate, it) })
        replaces.add(Function { replaceDate(timePair.first, timePair.second, formatTime, it) })
        replaces.add(Function { replaceDate(dateTimePair.first, dateTimePair.second, formatDateTime, it) })

        replaces.addAll(listOf(serverNamePair, serverVersionPair, bukkitVersionPair,
            playersBansPair, ipBansPair, playersOnlinePair, playersSlotsPair)
            .map { pair -> Function { defaultReplace(pair.first, "${pair.second}", it) } })

    }

    private fun replaceDate(regex: Regex, value: LocalDateTime, format: String,  msg: String) =
        regex.replace(msg, value.format(DateTimeFormatter.ofPattern(format)))


}