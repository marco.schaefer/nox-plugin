package br.com.schaefer.nox.processors.ping

import br.com.schaefer.nox.PlaceholderKeyword.PLAYERS_ONLINE
import br.com.schaefer.nox.PlaceholderKeyword.SLOTS
import br.com.schaefer.nox.config.NoxConfig
import br.com.schaefer.nox.processors.TextFormatProcessor
import com.comphenix.protocol.wrappers.WrappedServerPing

class PingVersionProcessor(private val ping: WrappedServerPing) : PingProcessor() {

    private val defaultSlots = "&7%$PLAYERS_ONLINE%&8/&7%$SLOTS%"

    private val customVersionEnabled = NoxConfig.CUSTOM_VERSION_ENABLED.getValue<Boolean>() ?: false
    private val value = NoxConfig.CUSTOM_VERSION.getValue<String>() ?: ""

    private val hiddenSlots = NoxConfig.SLOTS_HIDDEN.getValue<Boolean>() ?: false
    private val customSlotsEnabled = NoxConfig.CUSTOM_SLOTS_ENABLED.getValue<Boolean>() ?: false
    private val customSlotsValue = NoxConfig.CUSTOM_SLOTS.getValue<String>()

    override val enabled = customVersionEnabled || customSlotsEnabled

    override fun execute() {
        ping.versionProtocol = -1
        ping.versionName = getValue()
    }

    fun getValue() = value
        .let { if (!hiddenSlots) "$it ${getSlot()}" else it }
        .let { TextFormatProcessor().formatText(it) }

    private fun getSlot() =
        if (customSlotsEnabled)
            customSlotsValue
        else
            defaultSlots

}
