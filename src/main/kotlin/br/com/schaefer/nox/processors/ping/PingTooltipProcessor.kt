package br.com.schaefer.nox.processors.ping

import br.com.schaefer.nox.config.NoxConfig
import br.com.schaefer.nox.processors.TextFormatProcessor
import com.comphenix.protocol.wrappers.WrappedGameProfile
import com.comphenix.protocol.wrappers.WrappedServerPing
import java.util.*
import kotlin.collections.ArrayList


class PingTooltipProcessor(private val ping: WrappedServerPing) : PingProcessor() {

    override val enabled = NoxConfig.CUSTOM_TOOLTIP_SLOTS_ENABLED.getValue<Boolean>() ?: false
    private val message = NoxConfig.CUSTOM_TOOLTIP_SLOTS.getValue<ArrayList<String>>() ?: mutableListOf()

    override fun execute() {
        ping.setPlayers(message.map { WrappedGameProfile(UUID.randomUUID(), formatText(it)) })
    }

    private fun formatText(text: String) = TextFormatProcessor().formatText(text)

}