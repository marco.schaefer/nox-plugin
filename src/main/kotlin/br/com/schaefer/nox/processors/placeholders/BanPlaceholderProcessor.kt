package br.com.schaefer.nox.processors.placeholders

import br.com.schaefer.nox.PlaceholderKeyword.BAN_DATE
import br.com.schaefer.nox.PlaceholderKeyword.BAN_EXPIRES
import br.com.schaefer.nox.PlaceholderKeyword.BAN_REASON
import br.com.schaefer.nox.PlaceholderKeyword.BAN_SOURCE
import br.com.schaefer.nox.config.NoxConfig
import org.bukkit.BanEntry
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter
import java.util.*
import java.util.function.Function

class BanPlaceholderProcessor(banEntry: BanEntry) : PlaceholderProcessor() {

    private val reasonPair = getPlaceholder(BAN_REASON) to banEntry.reason
    private val datePair = getPlaceholder(BAN_DATE) to banEntry.source
    private val sourcePair = getPlaceholder(BAN_SOURCE) to banEntry.created
    private val expiresPair = getPlaceholder(BAN_EXPIRES) to banEntry.expiration

    init {
        replaces.add(Function { defaultReplace(reasonPair.first, reasonPair.second ?: "", it) })
        replaces.add(Function { defaultReplace(datePair.first, datePair.second, it) })
        replaces.add(Function { replaceDate(sourcePair.first, sourcePair.second, it) })
        replaces.add(Function { replaceExpiration(expiresPair.first, expiresPair.second, it) })
    }

    private fun replaceExpiration(regex: Regex, value: Date?, msg: String): String {
        return if (value != null) replaceDate(regex, value, msg) else defaultReplace(regex, "", msg)
    }

    private fun replaceDate(regex: Regex, value: Date, msg: String): String {
        val date = value.toInstant().atOffset(ZoneOffset.UTC)
        val formatDate = NoxConfig.FORMAT_EXPIRATION_DATE.getValue<String>()
        val formatter = DateTimeFormatter.ofPattern("$formatDate")

        return regex.replace(msg, date.format(formatter))
    }

}