package br.com.schaefer.nox.processors.ping

import br.com.schaefer.nox.config.NoxConfig
import com.comphenix.protocol.wrappers.WrappedServerPing

class PingHiddenSlotsProcessor(private val ping: WrappedServerPing) : PingProcessor() {

    override val enabled = NoxConfig.SLOTS_HIDDEN.getValue<Boolean>() ?: false

    override fun execute() {
        ping.isPlayersVisible = !enabled
    }
}