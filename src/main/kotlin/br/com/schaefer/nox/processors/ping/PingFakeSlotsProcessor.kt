package br.com.schaefer.nox.processors.ping

import br.com.schaefer.nox.config.NoxConfig
import br.com.schaefer.nox.providers.ServerSlotsProvider
import com.comphenix.protocol.wrappers.WrappedServerPing

class PingFakeSlotsProcessor(private val ping: WrappedServerPing) : PingProcessor() {

    override val enabled = NoxConfig.FAKE_SERVER_SLOTS_ENABLED.getValue<Boolean>() ?: false

    override fun execute() {
        ping.playersMaximum = ServerSlotsProvider().getFakeSlots()
    }

}