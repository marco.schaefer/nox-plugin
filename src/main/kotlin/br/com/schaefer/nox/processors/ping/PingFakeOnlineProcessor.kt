package br.com.schaefer.nox.processors.ping

import br.com.schaefer.nox.config.NoxConfig
import br.com.schaefer.nox.providers.PlayersOnlineProvider
import com.comphenix.protocol.wrappers.WrappedServerPing

class PingFakeOnlineProcessor(private val ping: WrappedServerPing) : PingProcessor() {

    private val fakeOnlineFixedEnabled = NoxConfig.FAKE_ONLINE_FIXED_ENABLED.getValue<Boolean>() ?: false
    private val fakeOnlinePlusEnabled = NoxConfig.FAKE_ONLINE_PLUS_ENABLED.getValue<Boolean>() ?: false
    private val fakeOnlineMultiplyEnabled = NoxConfig.FAKE_ONLINE_MULTIPLY_ENABLED.getValue<Boolean>() ?: false

    override val enabled = fakeOnlineFixedEnabled || fakeOnlinePlusEnabled || fakeOnlineMultiplyEnabled

    override fun execute() {
        ping.playersOnline = PlayersOnlineProvider().getFakeOnlinePlayer()
    }
}