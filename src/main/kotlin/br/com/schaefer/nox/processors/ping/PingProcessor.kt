package br.com.schaefer.nox.processors.ping

abstract class PingProcessor {

    abstract val enabled : Boolean

    abstract fun execute()

    fun apply() {
        if (enabled) execute()
    }

}