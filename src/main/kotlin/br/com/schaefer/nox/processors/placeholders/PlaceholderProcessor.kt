package br.com.schaefer.nox.processors.placeholders

import java.util.function.Function

abstract class PlaceholderProcessor {

    val replaces = mutableListOf<Function<String, String>>()

    private val delimiterChar = '%'

    fun replace(msg: String) = replaces.fold(msg) {str, replace -> replace.apply(str)}

    fun defaultReplace(regex: Regex, value:String, msg: String): String = regex.replace(msg, value)

    fun getPlaceholder(placeholder: String) = "${delimiterChar}${placeholder}${delimiterChar}".toRegex()

}