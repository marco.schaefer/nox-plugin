package br.com.schaefer.nox.processors

import br.com.schaefer.nox.hexresolver.HexResolver
import br.com.schaefer.nox.processors.placeholders.CustomPlaceholderProcessor
import br.com.schaefer.nox.processors.placeholders.PlaceholderProcessor
import br.com.schaefer.nox.processors.placeholders.SystemPlaceholderProcessor
import org.bukkit.ChatColor

class TextFormatProcessor {

    fun formatText(text: String?, customProcessMessage : PlaceholderProcessor?) : String {
        return breakLine((text ?: ""))
            .let { SystemPlaceholderProcessor().replace(it) }
            .let { CustomPlaceholderProcessor().replace(it) }
            .let { customProcessMessage?.replace(it) ?: it }
            .let { ChatColor.translateAlternateColorCodes('&', it) }
            .let { HexResolver().parseHexString(it) }
    }

    private fun breakLine(it: String) = it.replace("\\n", "\n")

    fun formatText(text: String?) : String {
        return formatText(text, null)
    }
}