package br.com.schaefer.nox.adapters

import br.com.schaefer.nox.providers.PlayersOnlineProvider
import br.com.schaefer.nox.providers.ServerSlotsProvider
import br.com.schaefer.nox.processors.ping.*
import com.comphenix.protocol.events.PacketAdapter
import com.comphenix.protocol.events.PacketEvent

class NoxAdapter(params: AdapterParameteters) : PacketAdapter(params) {

    override fun onPacketSending(event: PacketEvent) {
        val ping = event.packet.serverPings.read(0)

        ping.playersMaximum = ServerSlotsProvider().getFakeSlots()
        ping.playersOnline = PlayersOnlineProvider().getFakeOnlinePlayer()

        PingFakeSlotsProcessor(ping).apply()
        PingVersionProcessor(ping).apply()
        PingTooltipProcessor(ping).apply()
    }

}