package br.com.schaefer.nox.commands.actions.slots

import br.com.schaefer.nox.commands.actions.core.NoxBooleanConfigAction
import br.com.schaefer.nox.config.NoxConfig

class FakeOnlineMultiplyEnabled: NoxBooleanConfigAction(NoxConfig.FAKE_ONLINE_MULTIPLY) {

    override val name = "fakeOnlineMultiplyEnabled"

    override val permission = "nox.motd"

}