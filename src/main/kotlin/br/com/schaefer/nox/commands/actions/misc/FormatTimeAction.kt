package br.com.schaefer.nox.commands.actions.misc

import br.com.schaefer.nox.commands.actions.core.NoxStringConfigAction
import br.com.schaefer.nox.config.NoxConfig

class FormatTimeAction: NoxStringConfigAction(NoxConfig.FORMAT_TIME) {

    override val name = "formatTime"

    override val permission = "nox.motd"

}