package br.com.schaefer.nox.commands

import br.com.schaefer.nox.Constants
import br.com.schaefer.nox.commands.actions.core.ActionResolver
import org.bukkit.command.Command
import org.bukkit.command.CommandSender
import org.bukkit.command.TabCompleter

class NoxCompleter : TabCompleter {

    private val actionResolver = ActionResolver()

    override fun onTabComplete(
        sender: CommandSender,
        command: Command,
        alias: String,
        args: Array<out String>
    ): List<String>? {
        return if (isNoxCommand(command) && args.size == 1)
            actionResolver.actions.map { it.name }
        else if (isNoxCommand(command) && args.size > 1)
            actionResolver.resolveActionName(args[0])?.getComplete()
        else null
    }

    private fun isNoxCommand(command: Command) = Constants.COMMAND_NAME.equals(command.name, true)
}