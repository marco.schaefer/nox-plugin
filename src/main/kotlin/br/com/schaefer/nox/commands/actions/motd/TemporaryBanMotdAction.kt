package br.com.schaefer.nox.commands.actions.motd

import br.com.schaefer.nox.commands.actions.core.NoxStringConfigAction
import br.com.schaefer.nox.config.NoxConfig

class TemporaryBanMotdAction: NoxStringConfigAction(NoxConfig.TEMPORARY_BAN_MOTD) {

    override val name = "temporaryBanMotd"

    override val permission = "nox.motd"

}