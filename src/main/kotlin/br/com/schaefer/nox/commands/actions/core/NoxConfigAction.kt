package br.com.schaefer.nox.commands.actions.core

import br.com.schaefer.nox.Nox
import br.com.schaefer.nox.config.NoxConfig
import br.com.schaefer.nox.processors.TextFormatProcessor
import org.bukkit.command.CommandSender

abstract class NoxConfigAction(private val config: NoxConfig) : NoxAction() {

    override fun execute(sender: CommandSender, args: Array<out String>): Boolean {
        return if (isList(args)) {
            list(sender, args)
            true
        } else {
            getValueToPersist(args)?.also { persistConfig(it) }?.let { true } ?: false
        }
    }

    open fun isList(args: Array<out String>): Boolean = args.size == 1

    open fun list(sender: CommandSender, args: Array<out String>) {
        sender.sendMessage(TextFormatProcessor().formatText("[${config.keyWithPrefix}] = ${config.getValue<Any>()}"))
    }

    abstract fun getValueToPersist(args: Array<out String>): Any?

    private fun persistConfig(value: Any?) {
        Nox.instance.config.set(config.keyWithPrefix, value)
        Nox.instance.saveConfig()
        Nox.instance.loadConfig()
    }
}