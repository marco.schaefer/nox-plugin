package br.com.schaefer.nox.commands.actions.slots

import br.com.schaefer.nox.commands.actions.core.NoxBooleanConfigAction
import br.com.schaefer.nox.config.NoxConfig

class FakeOnlinePlusEnabled: NoxBooleanConfigAction(NoxConfig.FAKE_ONLINE_PLUS_ENABLED) {

    override val name = "fakeOnlinePlusEnabled"

    override val permission = "nox.motd"

}