package br.com.schaefer.nox.commands.actions.core

import br.com.schaefer.nox.config.NoxConfig

abstract class NoxStringConfigAction(config: NoxConfig) : NoxConfigAction(config) {

    override fun getValueToPersist(args: Array<out String>) =
        args.filterIndexed { index, _ -> index > 0 }.joinToString(" ")

}