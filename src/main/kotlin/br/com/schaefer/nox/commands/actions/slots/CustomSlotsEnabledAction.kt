package br.com.schaefer.nox.commands.actions.slots

import br.com.schaefer.nox.commands.actions.core.NoxBooleanConfigAction
import br.com.schaefer.nox.config.NoxConfig

class CustomSlotsEnabledAction: NoxBooleanConfigAction(NoxConfig.CUSTOM_SLOTS_ENABLED) {

    override val name = "customServerSlotsEnabled"

    override val permission = "nox.motd"

}