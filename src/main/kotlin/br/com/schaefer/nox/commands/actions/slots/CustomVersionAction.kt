package br.com.schaefer.nox.commands.actions.slots

import br.com.schaefer.nox.commands.actions.core.NoxStringConfigAction
import br.com.schaefer.nox.config.NoxConfig

class CustomVersionAction: NoxStringConfigAction(NoxConfig.CUSTOM_VERSION) {

    override val name = "customServerVersion"

    override val permission = "nox.motd"

}