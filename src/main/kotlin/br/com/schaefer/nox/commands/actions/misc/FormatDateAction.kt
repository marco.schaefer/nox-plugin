package br.com.schaefer.nox.commands.actions.misc

import br.com.schaefer.nox.commands.actions.core.NoxStringConfigAction
import br.com.schaefer.nox.config.NoxConfig

class FormatDateAction: NoxStringConfigAction(NoxConfig.FORMAT_DATE) {

    override val name = "formatDate"

    override val permission = "nox.motd"

}