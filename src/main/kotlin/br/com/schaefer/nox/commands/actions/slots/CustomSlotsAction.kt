package br.com.schaefer.nox.commands.actions.slots

import br.com.schaefer.nox.commands.actions.core.NoxStringConfigAction
import br.com.schaefer.nox.config.NoxConfig

class CustomSlotsAction: NoxStringConfigAction(NoxConfig.CUSTOM_SLOTS) {

    override val name = "customServerSlots"

    override val permission = "nox.motd"

}