package br.com.schaefer.nox.commands.actions.slots

import br.com.schaefer.nox.commands.actions.core.NoxIntConfigAction
import br.com.schaefer.nox.config.NoxConfig

class FakeOnlinePlus: NoxIntConfigAction(NoxConfig.FAKE_ONLINE_PLUS) {

    override val name = "fakeOnlinePlus"

    override val permission = "nox.motd"

}