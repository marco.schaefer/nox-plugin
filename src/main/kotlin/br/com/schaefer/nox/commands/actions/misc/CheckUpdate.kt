package br.com.schaefer.nox.commands.actions.misc

import br.com.schaefer.nox.UpdateChecker
import br.com.schaefer.nox.commands.actions.core.NoxAction
import org.bukkit.command.CommandSender

class CheckUpdate : NoxAction() {

    override val name = "checkUpdate"

    override val permission = "nox.check-update"

    private val updateChecker = UpdateChecker()

    override fun execute(sender: CommandSender, args: Array<out String>): Boolean {
        updateChecker.checkUpdate().let { pair -> pair.second.forEach { sendMessage(sender, it) } }
        return true
    }

}