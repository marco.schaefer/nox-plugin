package br.com.schaefer.nox.commands.actions.misc

import br.com.schaefer.nox.Nox
import br.com.schaefer.nox.commands.actions.core.NoxAction
import org.bukkit.command.CommandSender

class ReloadAction : NoxAction() {

    override val name = "reload"

    override val permission = "nox.reload"

    override var successMessage: String? = "&aNox plugin configs reloaded!"
    override fun execute(sender: CommandSender, args: Array<out String>): Boolean {
        Nox.instance.loadConfig()
        return true
    }
}