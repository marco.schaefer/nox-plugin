package br.com.schaefer.nox.commands.actions.motd

import br.com.schaefer.nox.commands.actions.core.NoxBooleanConfigAction
import br.com.schaefer.nox.config.NoxConfig

class BanMotdEnabledAction: NoxBooleanConfigAction(NoxConfig.BAN_MOTD_ENABLED) {

    override val name = "banMotdEnabled"

    override val permission = "nox.motd"

}