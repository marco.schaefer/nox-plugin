package br.com.schaefer.nox.commands.actions.core

object SubActionList {
    const val LIST = "list"
    const val ADD = "add"
    const val EDIT = "edit"
    const val DEL = "del"

    fun values() : List<String> = listOf(LIST, ADD, EDIT, DEL)
}