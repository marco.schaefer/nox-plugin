package br.com.schaefer.nox.commands.actions.slots

import br.com.schaefer.nox.commands.actions.core.NoxBooleanConfigAction
import br.com.schaefer.nox.config.NoxConfig

class TooltipSlotsEnabledAction: NoxBooleanConfigAction(NoxConfig.CUSTOM_TOOLTIP_SLOTS_ENABLED) {

    override val name = "tooltipSlotsEnabled"

    override val permission = "nox.motd"

}