package br.com.schaefer.nox.commands.actions.misc

import br.com.schaefer.nox.commands.actions.core.NoxBooleanConfigAction
import br.com.schaefer.nox.config.NoxConfig

class CheckUpdateJoinEnabledAction : NoxBooleanConfigAction(NoxConfig.CHECK_UPDATE_JOIN) {

    override val name = "checkUpdateJoinEnabled"

    override val permission = "nox.check-update"
}