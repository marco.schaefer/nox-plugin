package br.com.schaefer.nox.commands.actions.core

import br.com.schaefer.nox.processors.TextFormatProcessor
import br.com.schaefer.nox.processors.placeholders.PlaceholderProcessor
import org.bukkit.command.CommandSender

abstract class NoxAction {

    abstract val name: String

    abstract val permission: String

    open var successMessage : String? = null

    private fun hasPermission(sender: CommandSender) = sender.hasPermission(permission)

    abstract fun execute(sender: CommandSender, args: Array<out String>) : Boolean

    fun run(sender: CommandSender, args: Array<out String>): Boolean {
        return if (hasPermission(sender) && execute(sender, args)) {
            onSuccess(sender)
            true
        } else false
    }

    private fun onSuccess(sender: CommandSender) {
        successMessage?.let { sendMessage(sender, it) }
    }

    fun sendMessage(sender: CommandSender, message: String) {
        sender.sendMessage(formatMessage(message))
    }

    private fun formatMessage(message: String) : String {
        return TextFormatProcessor().formatText(message, customProcessMessage())
    }

    open fun customProcessMessage(): PlaceholderProcessor? = null

    open fun getComplete(args: Array<out String> = emptyArray()): List<String>? = null

}