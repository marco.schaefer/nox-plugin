package br.com.schaefer.nox.commands.actions.slots

import br.com.schaefer.nox.commands.actions.core.NoxIntConfigAction
import br.com.schaefer.nox.config.NoxConfig

class FakeOnlineMultiply: NoxIntConfigAction(NoxConfig.FAKE_ONLINE_FIXED) {

    override val name = "fakeOnlineMultiply"

    override val permission = "nox.motd"

}