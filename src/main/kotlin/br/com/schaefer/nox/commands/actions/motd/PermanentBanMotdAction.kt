package br.com.schaefer.nox.commands.actions.motd

import br.com.schaefer.nox.commands.actions.core.NoxStringConfigAction
import br.com.schaefer.nox.config.NoxConfig

class PermanentBanMotdAction: NoxStringConfigAction(NoxConfig.PERMANENT_BAN_MOTD) {

    override val name = "permanentBanMotd"

    override val permission = "nox.motd"

}