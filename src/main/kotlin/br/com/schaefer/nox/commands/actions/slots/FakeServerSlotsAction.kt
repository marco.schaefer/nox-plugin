package br.com.schaefer.nox.commands.actions.slots

import br.com.schaefer.nox.commands.actions.core.NoxIntConfigAction
import br.com.schaefer.nox.config.NoxConfig

class FakeServerSlotsAction: NoxIntConfigAction(NoxConfig.FAKE_SERVER_SLOTS) {

    override val name = "fakeServerSlots"

    override val permission = "nox.motd"

}