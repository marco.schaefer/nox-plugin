package br.com.schaefer.nox.commands.actions.slots

import br.com.schaefer.nox.commands.actions.core.NoxBooleanConfigAction
import br.com.schaefer.nox.config.NoxConfig

class CustomVersionEnabledAction: NoxBooleanConfigAction(NoxConfig.CUSTOM_VERSION_ENABLED) {

    override val name = "customServerVersionEnabled"

    override val permission = "nox.motd"

}