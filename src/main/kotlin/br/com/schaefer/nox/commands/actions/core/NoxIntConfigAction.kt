package br.com.schaefer.nox.commands.actions.core

import br.com.schaefer.nox.config.NoxConfig
import org.bukkit.command.CommandSender

abstract class NoxIntConfigAction(config: NoxConfig): NoxConfigAction(config) {

    override fun getValueToPersist(args: Array<out String>) = args[1].toInt()
}