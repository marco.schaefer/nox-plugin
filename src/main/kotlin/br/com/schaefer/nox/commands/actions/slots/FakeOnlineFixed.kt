package br.com.schaefer.nox.commands.actions.slots

import br.com.schaefer.nox.commands.actions.core.NoxIntConfigAction
import br.com.schaefer.nox.config.NoxConfig

class FakeOnlineFixed: NoxIntConfigAction(NoxConfig.FAKE_ONLINE_FIXED) {

    override val name = "fakeOnlineFixed"

    override val permission = "nox.motd"

}