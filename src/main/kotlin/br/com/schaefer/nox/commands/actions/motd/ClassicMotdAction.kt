package br.com.schaefer.nox.commands.actions.motd

import br.com.schaefer.nox.commands.actions.core.NoxStringConfigAction
import br.com.schaefer.nox.config.NoxConfig

class ClassicMotdAction: NoxStringConfigAction(NoxConfig.CLASSIC_MOTD) {

    override val name = "classicMotd"

    override val permission = "nox.motd"
}