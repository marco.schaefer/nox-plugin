package br.com.schaefer.nox.commands.actions.slots

import br.com.schaefer.nox.commands.actions.core.NoxBooleanConfigAction
import br.com.schaefer.nox.config.NoxConfig

class FakeOnlineFixedEnabled: NoxBooleanConfigAction(NoxConfig.FAKE_SERVER_SLOTS_ENABLED) {

    override val name = "fakeOnlineFixedEnabled"

    override val permission = "nox.motd"

}