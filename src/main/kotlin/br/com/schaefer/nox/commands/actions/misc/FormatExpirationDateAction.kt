package br.com.schaefer.nox.commands.actions.misc

import br.com.schaefer.nox.commands.actions.core.NoxStringConfigAction
import br.com.schaefer.nox.config.NoxConfig

class FormatExpirationDateAction: NoxStringConfigAction(NoxConfig.FORMAT_EXPIRATION_DATE) {

    override val name = "formatExtensionDate"

    override val permission = "nox.motd"

}