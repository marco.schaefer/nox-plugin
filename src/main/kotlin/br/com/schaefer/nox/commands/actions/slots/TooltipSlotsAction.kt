package br.com.schaefer.nox.commands.actions.slots

import br.com.schaefer.nox.commands.actions.core.NoxListConfigAction
import br.com.schaefer.nox.config.NoxConfig

class TooltipSlotsAction: NoxListConfigAction(NoxConfig.CUSTOM_TOOLTIP_SLOTS) {

    override val name = "tooltipSlots"

    override val permission = "nox.motd"

}