package br.com.schaefer.nox.commands.actions.motd

import br.com.schaefer.nox.commands.actions.core.NoxListConfigAction
import br.com.schaefer.nox.config.NoxConfig

class RandomMotdAction : NoxListConfigAction(NoxConfig.RANDOM_MOTD) {

    override val name = "randomMotd"

    override val permission = "nox.motd"

}