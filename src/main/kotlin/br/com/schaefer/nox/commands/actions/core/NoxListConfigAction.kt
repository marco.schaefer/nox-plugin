package br.com.schaefer.nox.commands.actions.core

import br.com.schaefer.nox.commands.actions.core.SubActionList.ADD
import br.com.schaefer.nox.commands.actions.core.SubActionList.DEL
import br.com.schaefer.nox.commands.actions.core.SubActionList.EDIT
import br.com.schaefer.nox.commands.actions.core.SubActionList.LIST
import br.com.schaefer.nox.config.NoxConfig
import org.bukkit.command.CommandSender

abstract class NoxListConfigAction(private val config: NoxConfig) : NoxConfigAction(config) {

    private val numberRegex = """\d*""".toRegex()

    override fun isList(args: Array<out String>): Boolean {
        return args.size == 1 || (args.size == 2 && (args[1] == LIST || isNumber(args[1])))
    }

    override fun list(sender: CommandSender, args: Array<out String>) {
        val list = config.getValue<MutableList<String>>()
        if (args.size == 2 && isNumber(args[1])) {
            list?.get(args[1].toInt())
        } else {
            list?.mapIndexed { i, s -> "[$i] $s" }?.joinToString("\n")
        }?.let { sendMessage(sender, it) }
    }

    private fun isNumber(str: String) = str.matches(numberRegex)

    override fun getValueToPersist(args: Array<out String>) = when (args[1]) {
        ADD -> add(args.filterIndexed { index, _ -> index > 1 }.joinToString(" "))
        DEL -> del(args[2].toInt())
        EDIT -> edit(args[2].toInt(), args.filterIndexed { index, _ -> index > 2 }.joinToString(" "))
        else -> null
    }

    private fun add(str: String): MutableList<String>? =
        config.getValue<MutableList<String>>()?.also { it.add(str) }

    private fun del(index: Int): MutableList<String>? =
        config.getValue<MutableList<String>>()?.also { it.removeAt(index) }

    private fun edit(index: Int, str: String): MutableList<String>? =
        config.getValue<MutableList<String>>()?.also { it[index] = str }

    override fun getComplete(args: Array<out String>) : List<String>? {
        return if (args.size == 2)
            SubActionList.values()
                .toMutableList()
                .also {
                    it.addAll(config.getValue<MutableList<String>>()
                        ?.mapIndexed { i, _ -> i.toString() }
                        ?: emptyList()) }
        else if (args.size == 3 && "^($EDIT|$DEL)$".toRegex().matches(args[1]))
            config.getValue<MutableList<String>>()
                ?.mapIndexed { i, _ -> i.toString()}
        else null
    }
}