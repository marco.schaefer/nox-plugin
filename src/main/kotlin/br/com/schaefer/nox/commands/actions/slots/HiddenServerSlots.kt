package br.com.schaefer.nox.commands.actions.slots

import br.com.schaefer.nox.commands.actions.core.NoxBooleanConfigAction
import br.com.schaefer.nox.config.NoxConfig

class HiddenServerSlots: NoxBooleanConfigAction(NoxConfig.SLOTS_HIDDEN) {

    override val name = "serverSlotsHidden"

    override val permission = "nox.motd"

}