package br.com.schaefer.nox.commands

import br.com.schaefer.nox.Constants.COMMAND_NAME
import br.com.schaefer.nox.commands.actions.core.ActionResolver
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender

class NoxCommand : CommandExecutor {

    private val actionResolver = ActionResolver()

    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
        if (isNoxCommand(command)) {
            if (args.isNotEmpty()) {
                return args.first()
                    .let { actionResolver.resolveActionName(it) }
                    ?.run(sender, args)
                    ?: false
            }
            return true
        }
        return false
    }

    private fun isNoxCommand(command: Command) = COMMAND_NAME.equals(command.name, true)

}