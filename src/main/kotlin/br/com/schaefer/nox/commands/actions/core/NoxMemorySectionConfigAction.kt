package br.com.schaefer.nox.commands.actions.core

import br.com.schaefer.nox.commands.actions.core.SubActionMemorySection.ADD
import br.com.schaefer.nox.commands.actions.core.SubActionMemorySection.DEL
import br.com.schaefer.nox.commands.actions.core.SubActionMemorySection.EDIT
import br.com.schaefer.nox.commands.actions.core.SubActionMemorySection.LIST
import br.com.schaefer.nox.config.NoxConfig
import br.com.schaefer.nox.processors.placeholders.CustomPlaceholderProcessor
import org.bukkit.command.CommandSender
import org.bukkit.configuration.MemorySection

abstract class NoxMemorySectionConfigAction(private val config: NoxConfig): NoxConfigAction(config) {

    override fun isList(args: Array<out String>): Boolean {
        return args.size == 1 || args.size == 2
    }

    override fun list(sender: CommandSender, args: Array<out String>) {
        val placeholders = config.getValue<MemorySection>()
        if (args.size == 2 && args[1] != LIST) {
            placeholders?.get(args[1])?.toString()
        } else {
            placeholders?.getKeys(true)
                ?.joinToString("\n") { "${CustomPlaceholderProcessor().buildKey(it)} -> ${placeholders.get(it)}" }
        }?.let { sendMessage(sender, it) }
    }

    override fun getValueToPersist(args: Array<out String>) = when (args[1]) {
        ADD -> addEdit(args[2], args.filterIndexed { index, _ -> index > 2 }.joinToString(" "))
        DEL -> del(args[2])
        EDIT -> addEdit(args[2], args.filterIndexed { index, _ -> index > 2 }.joinToString(" "))
        else -> null
    }

    private fun addEdit(key: String, value: String?): MemorySection? =
        config.getValue<MemorySection>()?.also { it[CustomPlaceholderProcessor().buildReverseKey(key)] = value }

    private fun del(key: String): MemorySection? = addEdit(key, null)

    override fun getComplete(args: Array<out String>) : List<String>? {
        return if (args.size == 2)
            SubActionMemorySection.values()
                .toMutableList()
                .also {
                    it.addAll(config.getValue<MemorySection>()
                    ?.getKeys(false)
                    ?.map(CustomPlaceholderProcessor()::buildKey)
                    ?: emptyList()) }
        else if (args.size == 3 && "^($EDIT|$DEL)$".toRegex().matches(args[1]))
            config.getValue<MemorySection>()
                ?.getKeys(false)
                ?.map(CustomPlaceholderProcessor()::buildKey)
        else null
    }

}