package br.com.schaefer.nox.commands.actions.core

import br.com.schaefer.nox.commands.actions.misc.*
import br.com.schaefer.nox.commands.actions.motd.*
import br.com.schaefer.nox.commands.actions.slots.*

class ActionResolver {

    val actions: MutableList<NoxAction> = mutableListOf()

    init {
        actions.addAll(listOf(
            ReloadAction(),
            CheckUpdate(),
            CheckUpdateJoinEnabledAction(),
            ClassicMotdAction(),
            RandomMotdEnabledAction(),
            RandomMotdAction(),
            BanMotdEnabledAction(),
            TemporaryBanMotdAction(),
            PermanentBanMotdAction(),
            HiddenServerSlots(),
            FakeServerSlotsEnabledAction(),
            FakeServerSlotsAction(),
            FakeOnlineFixedEnabled(),
            FakeOnlineFixed(),
            FakeOnlinePlusEnabled(),
            FakeOnlinePlus(),
            FakeOnlineMultiplyEnabled(),
            FakeOnlineMultiply(),
            CustomSlotsEnabledAction(),
            CustomSlotsAction(),
            TooltipSlotsEnabledAction(),
            TooltipSlotsAction(),
            CustomVersionEnabledAction(),
            CustomVersionAction(),
            FormatExpirationDateAction(),
            FormatDateAction(),
            FormatTimeAction(),
            CustomPlaceholderAction(),
        ))
    }

    fun resolveActionName(actionName: String): NoxAction? {
        return actions.find { it.name == actionName }
    }
}