package br.com.schaefer.nox.commands.actions.motd

import br.com.schaefer.nox.commands.actions.core.NoxBooleanConfigAction
import br.com.schaefer.nox.config.NoxConfig

class RandomMotdEnabledAction: NoxBooleanConfigAction(NoxConfig.RANDOM_MOTD_ENABLED) {

    override val name = "randomMotdEnabled"

    override val permission = "nox.motd"

}