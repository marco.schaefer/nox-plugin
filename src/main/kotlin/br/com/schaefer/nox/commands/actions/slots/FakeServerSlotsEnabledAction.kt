package br.com.schaefer.nox.commands.actions.slots

import br.com.schaefer.nox.commands.actions.core.NoxBooleanConfigAction
import br.com.schaefer.nox.config.NoxConfig

class FakeServerSlotsEnabledAction: NoxBooleanConfigAction(NoxConfig.FAKE_SERVER_SLOTS_ENABLED) {

    override val name = "fakeServerSlotsEnabled"

    override val permission = "nox.motd"

}