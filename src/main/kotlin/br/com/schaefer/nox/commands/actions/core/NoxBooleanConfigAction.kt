package br.com.schaefer.nox.commands.actions.core

import br.com.schaefer.nox.config.NoxConfig

abstract class NoxBooleanConfigAction(config: NoxConfig) : NoxConfigAction(config) {

    override fun getValueToPersist(args: Array<out String>) = if (args.size > 1) args[1].toBoolean() else false

    override fun getComplete(args: Array<out String>) = listOf("true", "false")
}