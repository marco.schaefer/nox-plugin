package br.com.schaefer.nox.commands.actions.misc

import br.com.schaefer.nox.commands.actions.core.NoxMemorySectionConfigAction
import br.com.schaefer.nox.config.NoxConfig

class CustomPlaceholderAction: NoxMemorySectionConfigAction(NoxConfig.CUSTOM_PLACEHOLDERS) {

    override val name = "customPlaceholder"

    override val permission = "nox.motd"

}