package br.com.schaefer.nox.litenters

import br.com.schaefer.nox.UpdateChecker
import br.com.schaefer.nox.config.NoxConfig
import br.com.schaefer.nox.processors.TextFormatProcessor
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerJoinEvent
import java.util.logging.Level

class PlayerJoinListener : Listener {

    private val updateChecker = UpdateChecker()

    @EventHandler(priority = EventPriority.HIGH)
    fun onLogin(event: PlayerJoinEvent) {
        val enabled = NoxConfig.CHECK_UPDATE_JOIN.getValue<Boolean>() ?: false
        if (enabled && event.player.isOp) {
            updateChecker.checkUpdate()
                .let { if (it.first != Level.WARNING) null else it }
                ?.also { pair -> pair.second.forEach {
                    event.joinMessage = TextFormatProcessor().formatText(it)
                } }
        }
    }

}