package br.com.schaefer.nox.litenters

import br.com.schaefer.nox.providers.ServerSlotsProvider
import br.com.schaefer.nox.providers.MessageOfTheDayProvider
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.server.ServerListPingEvent

class PingListener : Listener {

    @EventHandler(priority = EventPriority.HIGH)
    fun proxyPing(event : ServerListPingEvent) {
        event.maxPlayers = ServerSlotsProvider().getFakeSlots()
        MessageOfTheDayProvider()
            .getMessageOfTheDay(event)
            .getFormattedMessage()
            .also { event.motd = it }
    }

}