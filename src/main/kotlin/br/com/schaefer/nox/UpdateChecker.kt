package br.com.schaefer.nox

import com.google.gson.JsonParser
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.URL
import java.util.*
import java.util.logging.Level

class UpdateChecker {

    private val logger = Nox.instance.logger

    private val projectProperties = Properties()

    init {
        projectProperties.load(this.javaClass.getResourceAsStream("/project.properties"))
    }

    fun checkUpdate() = getCheckUpdateMessages().also { pair -> pair.second.forEach { logger.log(pair.first, it) } }

    private fun getCheckUpdateMessages() : Pair<Level, List<String>> {
        val currentVersion = getCurrentVersion()
        val latestStableVersion = getLatestStableVersion()

        if (latestStableVersion != null){
            return if (latestStableVersion > currentVersion) {
                Level.WARNING to listOf(
                    "Stable Version: &a$latestStableVersion&r is out! You are still running version: &c$currentVersion",
                    "Update at: ${Constants.COURSEFORGE_BUKKIT_PLUGINS_URI}/${Constants.COURSEFORGE_PROJECT_URI}")
            } else if (currentVersion > latestStableVersion) {
                Level.INFO to listOf("Stable Version: $latestStableVersion | Current Version: $currentVersion")
            } else {
                Level.INFO to listOf("&cNo new version available")
            }
        }
        return Level.INFO to listOf("&eThere was an issue attempting to check for the latest version.")
    }

    private fun getLatestStableVersion(): Version? {
        try {
            val url = URL("${Constants.COURSEFORGE_API_URI}/servermods/files?projectids=${Constants.COURSEFORGE_PROJECT_ID}")
            val conn = url.openConnection().also {
                it.readTimeout = 5000
                it.addRequestProperty("User-Agent", "NoxPlugin Update Checker")
                it.doOutput = true
            }
            val reader = BufferedReader(InputStreamReader(conn.getInputStream()))
            val response = reader.readLine()

            val parser = JsonParser()
            val array = parser.parse(response).asJsonArray
            if (array.size() == 0) {
                logger.warning("No files found, or Feed URL is bad.")
                return null
            }

            val regex = """^(\w*)-(\d*.\d*.\d*)$""".toRegex()

            return array.toList()
                .asSequence()
                .map { it.asJsonObject }
                .filter { it.get("releaseType").asString == "release" }
                .map { it.get("name").asString }
                .map { regex.find(it)!!.groupValues[2] }
                .map { Version.fromString(it) }
                .filterNotNull()
                .sorted()
                .toList()
                .first()
        } catch (e: Exception) {
            logger.info("There was an issue attempting to check for the latest version.")
        }
        return null
    }

    private fun getCurrentVersion() : Version {
        return Version.fromString(projectProperties.getProperty("version"))!!
    }
}